(function (document, window, $) {
  $(document).ready(function () {
    // Retrive data from form
    function getFormData(form) {
      var formData = new FormData(form);
      // Loop sur tout les entrers ...
      console.log(Object.fromEntries(formData));
    }
    // Triggers validation bootstrap
    const forms = document.querySelectorAll(".needs-validation");
    // Loop over them and prevent submission
    Array.from(forms).forEach((form) => {
      form.addEventListener(
        "submit",
        (event) => {
          if (!form.checkValidity()) {
            event.preventDefault();
            event.stopPropagation();
          }
          form.classList.add("was-validated");
        },
        false
      );
    });
    // Validation
 
    if(document.getElementById("contact-form")){
    const formContact = document.getElementById("contact-form");
    formContact
      .addEventListener("submit", function (e) {
        e.preventDefault();
        getFormData(e.target);
        const formValid = document.getElementById("contact-valide");
        if (formContact.checkValidity()) {
          formContact.classList.add("d-none");
          formValid.classList.remove("d-none");
        }
      });}
    $('[data-toggle="tooltip"]').tooltip();
  });
})(document, window, jQuery);

// btn-mobile
$(document).ready(function () {
  $(".btn-mobile").click(function () {
    $(".menu-page").slideToggle("show");
    $(this).toggleClass("open");
  });
});

//Slider Innovation Two
$("#slides-bourses").slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  centerMode: true,
  centerPadding: "60px",
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 500,
      settings: {
        centerPadding: "20px",
        arrows: true,
        dots: false,
        centerMode: true,
        slidesToShow: 1,
      },
    },
  ],
});

//slide temoignages
$("#slide-temoignage").slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  arrows: false,
  fade: true,
  centerPadding: "10px",
  asNavFor: "#slide-temoignage-nav",
});
$("#slide-temoignage-nav").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  asNavFor: "#slide-temoignage",
  focusOnSelect: true,
  dots: false,
  arrows: true,
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 500,
      settings: {
        slidesToShow: 1,
      },
    },
  ],
});

//Slider Innovation Two
$("#slides-Strategies").slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  centerMode: true,
  dots: true,
  centerPadding: "80px",
  responsive: [
    {
      breakpoint: 1025,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        centerPadding: "80px",
      },
    },
    {
      breakpoint: 769,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 500,
      settings: {
        centerPadding: "40px",
        arrows: true,
        dots: true,
        centerMode: true,
        slidesToShow: 1,
      },
    },
  ],
});




                // Actualites slide on mobile
                $("#slider-actualites").slick({
                    mobileFirst: true,
                    arrows: false,
                    centerPadding: "10px",
                    responsive: [
                        {
                            breakpoint: 2000,
                            settings: "unslick",
                        },
                        {
                            breakpoint: 1600,
                            settings: "unslick",
                        },
                        {
                            breakpoint: 1024,
                            settings: "unslick",
                        },
                        {
                            breakpoint: 767,
                            settings: {
                                centerPadding: "20px",
                                arrows: false,
                                dots: false,
                                centerMode: true,
                                slidesToShow:2,
                            },
                        },
                        {
                            breakpoint: 500,
                            settings: {
                                centerPadding: "10px",
                                arrows: false,
                                dots: false,
                                centerMode: true,
                                slidesToShow: 1.5,
                            },
                        },
                    ],
                });