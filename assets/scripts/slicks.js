$(".sliderBanner").slick({
  slidesToShow: 3,
  slidesToScroll: 1,
  arrows: true,
  dots: true,
  fade: false,
  infinite: true,
  pauseOnHover: true,
  focusOnSelect: true,
  appendDots: $(".sliderBanner-dots"),
  prevArrow: $(".sliderBanner-prev"),
  nextArrow: $(".sliderBanner-next"),
  centerPadding: 0,
  centerPadding: 0,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 768,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
      },
    },
  ],
});
// On before slide change
$(".sliderBanner").on(
  "beforeChange",
  function (event, { slideCount: count }, currentSlide, nextSlide) {
    let selectors = [nextSlide, nextSlide - count, nextSlide + count]
      .map((n) => `[data-slick-index="${n}"]`)
      .join(", ");
    $(".slick-now").removeClass("slick-now");
    $(selectors).addClass("slick-now");
  }
);
$('[data-slick-index="0"]').addClass("slick-now");

// Partenariat Slider
$(".sliderPartenariat").slick({
  dots: false,
  infinite: true,
  arrows: true,
  autoplay: true,
  autoplaySpeed: 3000,
  slidesToShow: 5,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: {
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
    {
      breakpoint: 768,
      settings: {
        arrows: false,
        centerMode: true,
        centerPadding: "0",
        slidesToShow: 3,
        slidesToScroll: 1,
      },
    },
  ],
});
// Programme Slider
$(".sliderProgramme").slick({
  dots: false,
  arrows: false,
  infinite:false,
  centerMode: true,
  centerPadding: "60px",
  mobileFirst: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 1024,
      settings: "unslick",
    },
  ],
});
// Temoignage Slider
$(".sliderTemoignage").slick({
  dots: false,
  infinite:false,
  arrows: false,
  centerMode: true,
  centerPadding: "35px",
  mobileFirst: true,
  focusOnSelect: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 768,
      settings: "unslick",
    },
  ],
});
// Actualité Slider
$(".sliderActualite").slick({
  dots: false,
  infinite:true,
  arrows: false,
  centerMode: true,
  centerPadding: "35px",
  mobileFirst: true,
  focusOnSelect: true,
  slidesToShow: 1,
  slidesToScroll: 1,
  responsive: [
    {
      breakpoint: 768,
      settings: "unslick",
    },
  ],
});

//slider actualite details
$("#slideraimeras").slick({
  slidesToShow: 4,
  slidesToScroll: 1,
  arrows: true,
  centerMode: true,
  dots:false,
  centerPadding: 0,
  responsive: [
     
      {
          breakpoint: 1025,
          settings: {
              slidesToShow: 3,
              slidesToScroll: 1,
              centerPadding: 0,
          },
      },
      {
          breakpoint: 769,
          settings: {
              slidesToShow: 2,
              slidesToScroll: 1,
          },
      },
      {
          breakpoint: 500,
          settings: {
              centerPadding: 0,
              arrows: false,
              dots: false,
              centerMode: true,
              slidesToShow: 1,
          },
      },
  ],
});
